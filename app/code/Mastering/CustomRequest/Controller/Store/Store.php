<?php

namespace  Mastering\CustomRequest\Controller\Store;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Mastering\CustomRequest\Model\CustomProduct;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Message\ManagerInterface as MessageManager;

class Store extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface

{
    protected $_pageFactory;
    protected $customProduct;
    protected $messageManager;

    public function __construct(

        Context $context,
        PageFactory $pageFactory,
        CustomProduct $customProduct,
        MessageManager $messageManager
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->customProduct = $customProduct;
        $this->messageManager = $messageManager;

        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            // Retrieve form data
            $postData = $this->getRequest()->getPostValue();
            $objManger = \Magento\Framework\App\ObjectManager::getInstance();
            $logger = $objManger->create('\Psr\Log\LoggerInterface');

            $logger->info('this is from custom request class');
            $logger->info($postData);
            // Create a new instance of the model
            $customModel = $this->customProduct;

            // Set data for the model
            $customModel->setData($postData);

            // Save the model data to the database
            $customModel->save();

            // Display a success message
            $this->messageManager->addSuccessMessage(__('Your request has been submitted successfully.'));

            // Redirect to a thank you page or any other desired page
            $this->_redirect('/');
        } catch (\Exception $e) {
            // Handle errors
            $this->messageManager->addErrorMessage(__('An error occurred while processing your request.'));
            $this->_redirect('/customrequest/index/index'); // Redirect back to the form page
        }
    }
}
