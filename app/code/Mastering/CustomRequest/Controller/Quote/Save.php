<?php

namespace Mastering\CustomRequest\Controller\Quote;
use Magento\Sales\Api\OrderRepositoryInterface;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $quoteIdMaskFactory;

    protected $quoteRepository;

    protected $orderRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        OrderRepositoryInterface $orderrepository
    ) {
        parent::__construct($context);
        $this->quoteRepository = $quoteRepository;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->orderRepository = $orderrepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if ($post) {
            $cartId       = $post['cartId'];
            $deliveryNotes = $post['delivery_notes'];
            $loggin       = $post['is_customer'];

            if ($loggin === 'false') {
                $cartId = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id')->getQuoteId();
            }

            $quote = $this->quoteRepository->getActive($cartId);
            if (!$quote->getItemsCount()) {
                throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
            }

            $quote->setData('delivery_notes', $deliveryNotes);
            $this->quoteRepository->save($quote);

            $order = $this->orderRepository->get($quote->getReservedOrderId());

            $objManger = \Magento\Framework\App\ObjectManager::getInstance();
            $logger = $objManger->create('\Psr\Log\LoggerInterface');

            $logger->info('this is from custom request class'.$order);
//            if ($order) {
//                $order->setData('delivery_notes', $deliveryNotes);
//                $this->orderRepository->save($order);
//            }
        }
        return;
    }
}
