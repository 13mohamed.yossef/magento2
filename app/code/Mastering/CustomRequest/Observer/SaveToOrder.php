<?php
namespace Mastering\CustomRequest\Observer;
class SaveToOrder implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $quote = $event->getQuote();
        $order = $event->getOrder();
        var_dump($order);
        $order->setData('delivery_notes', $quote->getData('delivery_notes'));
    }
}
