<?php

namespace Mastering\CustomRequest\Cron;

use Mastering\CustomRequest\Model\CustomProduct;
use Mastering\CustomRequest\Model\Config;

class AddItem
{
    private $itemFactory;

    private $config;

    public function __construct(CustomProduct $itemFactory, Config $config)
    {
        $this->itemFactory = $itemFactory;
        $this->config = $config;
    }

    public function execute()
    {
        if ($this->config->isEnabled()) {
            $this->itemFactory->create()
                ->setName('Scheduled item')
                ->setDescription('Created at ' . time())
                ->save();
        }
    }
}
