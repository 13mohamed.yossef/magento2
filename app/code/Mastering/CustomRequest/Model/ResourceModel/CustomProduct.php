<?php

namespace Mastering\CustomRequest\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CustomProduct extends AbstractDb {

    protected function _construct(): void
    {
        $this->_init('customers_custom_request', 'id');
    }
}
