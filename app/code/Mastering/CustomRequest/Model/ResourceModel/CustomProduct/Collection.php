<?php

namespace Mastering\CustomRequest\Model\ResourceModel\CustomProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection {

    protected $_idFieldName = 'id' ;

    protected function _construct(): void
    {
        $this->_init(
                'Mastering\CustomRequest\Model\CustomProduct',
            'Mastering\CustomRequest\Model\ResourceModel\CustomProduct');
    }
    
}
