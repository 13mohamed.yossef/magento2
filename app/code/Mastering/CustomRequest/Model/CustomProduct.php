<?php


namespace Mastering\CustomRequest\Model;

use Magento\Framework\Model\AbstractModel;

class CustomProduct extends AbstractModel
{
    protected function _construct(): void
    {
        $this->_init('Mastering\CustomRequest\Model\ResourceModel\CustomProduct');
    }
}
