<?php

namespace Mastering\CustomRequest\Model;

use Mastering\CustomRequest\Api\CustomProductRepositoryInterface;
use Mastering\CustomRequest\Api\Data;
use Mastering\CustomRequest\Model\ResourceModel\CustomProduct\Collection;


class CustomProductRepository implements CustomProductRepositoryInterface {

    private $collectionFactory;
    public function __constructor(Collection $collectionFactory){
        $this->collectionFactory = $collectionFactory;
    }

    public function getItems(): array
    {
        return $this->collectionFactory->getItems();
    }


    public function getItemById(string $id): \Mastering\CustomRequest\Api\Data\CustomProductInterface
    {
        return $this->collectionFactory->getItemById($id);
    }
}
