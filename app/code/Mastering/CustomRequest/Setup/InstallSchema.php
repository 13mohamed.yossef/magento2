<?php

namespace Mastering\CustomRequest\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface{

    /**
     * @throws \Zend_Db_Exception
     */


    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {

        $objManger = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objManger->create('\Psr\Log\LoggerInterface');

        $logger->info('this is from custom request class');

        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('customers_custom_request')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'identity' => true]
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            50,
            ['nullable' => false]
        )->addColumn(
            'age',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true]
        )->addColumn(
            'phone_number',
            Table::TYPE_TEXT,
            15,
            ['nullable' => true]
        )->addColumn(
            'height',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false]
        )->addColumn(
            'weight',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false]
        )->addColumn(
            'order_notes',
            Table::TYPE_TEXT,
            50,
            ['nullable' => true]
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT]
        )->setComment('custom request for products');

        $setup->getConnection()->createTable($table);

        $logger->info('this is from custom request class after table creation');

        $setup->endSetup();
    }

}
