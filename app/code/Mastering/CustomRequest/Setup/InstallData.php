<?php

namespace Mastering\CustomRequest\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements  InstallDataInterface {

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        $setup->getConnection()->insert(
            'customers_custom_request',
            ['name' => 'mohamed yossef', 'height' => 187, 'weight' => 76]

        );

        $setup->endSetup();
    }
}

