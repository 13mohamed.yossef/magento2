<?php

namespace Mastering\CustomRequest\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;


class UpgradeSchema implements  UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();


        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'delivery_notes',
            [
                'type' => Table::TYPE_TEXT ,
                'nullable' => true,
                'comment' => 'delivery notes'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'delivery_notes',
            [
                'type' => Table::TYPE_TEXT ,
                'nullable' => true,
                'comment' => 'delivery notes'
            ]
        );

        $setup->endSetup();
    }
}
