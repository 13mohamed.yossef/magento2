<?php

namespace  Mastering\CustomRequest\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;


class Store extends Template{

    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getMessage(): string
    {
        return 'Your data submitted successfully' ;
    }

}
