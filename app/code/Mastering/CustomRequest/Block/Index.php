<?php

namespace  Mastering\CustomRequest\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mastering\CustomRequest\Model\ResourceModel\CustomProduct\Collection;


class Index extends Template{

    private  $collectionFactory;

    public function __construct(Context $context, Collection $collectionFactory, array $data = [])
    {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
    }
//    public function getProducts(){
//        return $this->collectionFactory->getItems();
//    }

    public function getFormAction()
    {
        return $this->getUrl('/customeproduct/store/store', ['_secure' => true]);
    }
}
