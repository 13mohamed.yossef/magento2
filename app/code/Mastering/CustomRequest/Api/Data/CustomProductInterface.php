<?php

namespace Mastering\CustomRequest\Api\Data;


interface CustomProductInterface {

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getHeight(): int;

    /**
     * @return int
     */
    public function getWeight(): int;

    /**
     * @return int | null
     */
    public function getAge(): ?int;

    /**
     * @return string | null
     */
    public function getPhone(): ?string;


    /**
     * @return string | null
     */
    public function getOrder(): ?string;
}
