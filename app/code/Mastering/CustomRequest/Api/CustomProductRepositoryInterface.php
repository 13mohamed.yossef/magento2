<?php


namespace Mastering\CustomRequest\Api;


interface CustomProductRepositoryInterface {

    /**
     * @return \Mastering\CustomRequest\Api\Data\CustomProductInterface[]
     */
    public function getItems(): array;

    /**
     *
     * @param string $id
     *@return \Mastering\CustomRequest\Api\Data\CustomProductInterface
     */
    public function getItemById(string $id): \Mastering\CustomRequest\Api\Data\CustomProductInterface;
}
