<?php

namespace Mastering\SampleModule\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;


class UpgradeSchema implements  UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        $setup->getConnection()->addColumn(
            $setup->getTable('mastering_item_table'),
            'description',
            [
                'type' => Table::TYPE_TEXT ,
                'nullable' => true,
                'comment' => 'items description'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'sales_notes',
            [
                'type' => Table::TYPE_TEXT ,
                'nullable' => true,
                'comment' => 'sales notes'
            ]
        );


        $setup->endSetup();
    }
}
