<?php

namespace  Mastering\SampleModule\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class  UpgradeData implements UpgradeDataInterface {

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        if (version_compare($context->getVersion() , '1.0.1', '<')) {
            $setup->getConnection()->update(
                $setup->getTable('mastering_item_table'),
                ['description' => 'default'],
                $setup->getConnection()->quoteInto('id = ?' , 1)
            );
        }

        $setup->endSetup();
    }
}

